// otel.js
const { NodeSDK } = require('@opentelemetry/sdk-node');
const { HttpTraceContext } = require('@opentelemetry/core');
const { getNodeAutoInstrumentations } = require('@opentelemetry/auto-instrumentations-node');
const { OTLPTraceExporter } = require('@opentelemetry/exporter-trace-otlp-http');

// Initialize OpenTelemetry SDK
const sdk = new NodeSDK({
  traceExporter: new OTLPTraceExporter({
    url: 'http://<otel-collector-service>:55681/v1/traces', // Update with your OpenTelemetry Collector endpoint
  }),
  instrumentations: [getNodeAutoInstrumentations()],
});

sdk.start().then(() => {
  console.log('OpenTelemetry initialized');
}).catch(error => {
  console.error('Error initializing OpenTelemetry', error);
});
