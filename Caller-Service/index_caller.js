const axios = require('axios');
const express = require('express');

const app = express();
const port = 3001;

// Middleware to parse JSON bodies
app.use(express.json());

const greetingsServiceUrl = process.env.CALLER_SERVICE_URL || 'http://greetings-service:80';


// Health check endpoint
app.get('/health', (req, res) => {
    res.status(200).send('OK');
});

const createGreeting = async (message) => {
    try {
        const response = await axios.post(`${greetingsServiceUrl}/greetings`, { message });
        console.log('Greeting created:', response.data);
        return response.data.id;
    } catch (error) {
        console.error('Error creating greeting:', error.message);
        console.error(error.response ? error.response.data : error);
    }
};

const getGreeting = async (id) => {
    try {
        const response = await axios.get(`${greetingsServiceUrl}/greetings/${id}`);
        console.log('Greeting:', response.data);
    } catch (error) {
        console.error('Error getting greeting:', error.message);
        console.error(error.response ? error.response.data : error);
    }
};

const updateGreeting = async (id, message) => {
    try {
        const response = await axios.put(`${greetingsServiceUrl}/greetings/${id}`, { message });
        console.log('Greeting updated:', response.data);
    } catch (error) {
        console.error('Error updating greeting:', error.message);
        console.error(error.response ? error.response.data : error);
    }
};

const deleteGreeting = async (id) => {
    try {
        const response = await axios.delete(`${greetingsServiceUrl}/greetings/${id}`);
        console.log('Greeting deleted:', response.data);
    } catch (error) {
        console.error('Error deleting greeting:', error.message);
        console.error(error.response ? error.response.data : error);
    }
};

app.listen(port, () => {
    console.log(`Caller-Service listening at http://localhost:${port}`);
});

const run_success = async () => {
    const id = await createGreeting('Hello, World!');
    if (id) {
        await getGreeting(id);
        await updateGreeting(id, 'Hello, Universe!');
        await deleteGreeting(id);
    } else {
        console.error('Failed to create greeting, skipping subsequent operations.');
    }
};



// POST /trigger endpoint
app.post('/trigger_success', async (req, res) => {
    res.sendStatus(202);
    try {
        await run_success(); // Run the asynchronous operations
        console.log('Caller-Service: Trigger_success trace operations completed successfully.');
    } catch (error) {
        console.error('Error during trigger_success operations:', error.message);
        // Handle the error appropriately
    }
});

// Endpoint to trigger /doublethree
app.get('/trigger_doublethree', async (req, res) => {
    try {
        console.log('Caller: Doublethree begin')
        // Make a GET request to /doublethree in the greetings-service
        const response = await axios.get(`${greetingsServiceUrl}/doublethree`);
        console.log('Response from /doublethree:', response.data);
        res.send(`Response from /doublethree: ${response.data}`);
    } catch (error) {
        console.error('Error calling /doublethree:', error.message);
        res.status(500).send('Error calling /doublethree');
    }
});

// Endpoint to trigger /doubleseven
app.get('/trigger_doubleseven', async (req, res) => {
    try {
        // Make a GET request to /doubleseven in the greetings-service
        console.log('Caller: Doubleseven begin')
        const response = await axios.get(`${greetingsServiceUrl}/doubleseven`);
        console.log('Response from /doubleseven:', response.data);
        res.send(`Response from /doubleseven: ${response.data}`);
    } catch (error) {
        console.error('Error calling /doubleseven:', error.message);
        res.status(500).send('Error calling /doubleseven');
    }
});

// Endpoint to trigger /inconsistentNaming
app.post('/triggerInconsistentNaming_caps', async (req, res) => {
    // Extract MYVAR from the request body
    const { myvar } = req.body;
    console.log(`Received POST request triggerInconsistentNaming_caps for: ${myvar}`);
    // Create a new object with 'MYVAR' as the key
    const data = {
        MYVAR: myvar
    };

    try {
        // Send the data to the greetings-service
        const response = await axios.post(`${greetingsServiceUrl}/inconsistentNaming`, data);
        res.status(200).send(response.data);
    } catch (error) {
        console.error('Error calling greetings-service:', error.message);
        res.status(500).send('Error calling greetings-service');
    }
});
// Endpoint to trigger /inconsistentNaming
app.post('/triggerInconsistentNaming_lower', async (req, res) => {
    // Extract MYVAR from the request body
    const { myvar } = req.body;
    console.log(`Received POST request triggerInconsistentNaming_lower for: ${myvar}`);
    // Create a new object with 'MYVAR' as the key
    const data = {
        myvar: myvar
    };
    try {
        // Send the data to the greetings-service
        const response = await axios.post(`${greetingsServiceUrl}/inconsistentNaming`, data);
        res.status(200).send(response.data);
    } catch (error) {
        console.error('Error calling greetings-service:', error.message);
        res.status(500).send('Error calling greetings-service');
    }
});