const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// MongoDB connection details
const dbUsername = process.env.MONGO_USERNAME || 'admin';
const dbPassword = process.env.MONGO_PASSWORD || 'admin';
const dbHost = process.env.MONGO_HOST || 'mongodb-service';
const dbPort = process.env.MONGO_PORT || '27017';
const dbName = process.env.MONGO_DB || 'mydatabase';
const mongoURI = `mongodb://${dbUsername}:${dbPassword}@${dbHost}:${dbPort}/${dbName}?authSource=admin`;

const app = express();
const port = process.env.PORT || 3000; // Use environment variable for port

app.use(express.json());

mongoose.connect(process.env.MONGO_URI || `${mongoURI}`, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
})
.then(() => console.log('Connected to MongoDB'))
.catch(err => console.error('Could not connect to MongoDB', err));


const rowSchema = new mongoose.Schema({
    name: String,
    value: Number
});
const Row = mongoose.model('Row', rowSchema);

app.use(bodyParser.json());

let greetings = {};

//GET /doublethree
//Upload 6 to DB to row1, wait 1 min, get value  of row1 from db, return value of row1. Used to trigger racecondition
// /doublethree endpoint
app.get('/doublethree', async (req, res) => {
    try {
        // Write the value 6 to row1
        await Row.findOneAndUpdate(
            { name: 'row1' },
            { value: 6 },
            { upsert: true, new: true }
        );

        // Delay for 5 seconds
        await new Promise(resolve => setTimeout(resolve, 5000));

        // Get the value of row1 from the database
        const row = await Row.findOne({ name: 'row1' });
        res.send(`The value of row1 is ${row.value}`);
    } catch (error) {
        console.error('Error in /doublethree:', error);
        res.status(500).send('An error occurred');
    }
});
//GET /doubleseven
//Upload 14 to DB to row1, get value of row1 from db, return value of row1. Used to trigger racecondition
app.get('/doubleseven', async (req, res) => {
    try {
        // Write the value 14 to row1
        await Row.findOneAndUpdate(
            { name: 'row1' },
            { value: 14 },
            { upsert: true, new: true }
        );

        // Immediately get the value of row1 from the database
        const row = await Row.findOne({ name: 'row1' });
        res.send(`The value of row1 is ${row.value}`);
    } catch (error) {
        console.error('Error in /doubleseven:', error);
        res.status(500).send('An error occurred');
    }
});

//POST /triggerInconsistentNaming
//Returns Hi if the body of req is 'myvar':5, else Bye. Used to trigger casesensitivity in variables. 'MYVAR':5 is sent and always returns Bye
app.post('/inconsistentNaming', (req, res) => {
    const { myvar } = req.body; // Extract myvar from the request body
    console.log(`Received POST request InconsistentNaming for: ${myvar}`);
    if (myvar === '5') {
        res.status(200).send('Hi');
    } else {
        res.status(200).send('Bye');
    }
});

const server = app.listen(port, () => {
    console.log(`GreetingsService listening at http://localhost:${port}`);
});

// Health check endpoint
app.get('/health', (req, res) => {
    res.status(200).send({ status: 'UP' });
    console.log('Health check endpoint was called');
});

// GET /greetings/:id endpoint
app.get('/greetings/:id', (req, res) => {
    const id = req.params.id;
    console.log(`Received GET request for id: ${id}`);
    if (greetings[id]) {
        res.status(200).send(greetings[id]);
    } else {
        res.status(404).send({ error: 'Greeting not found' });
    }
});

// PUT /greetings/:id endpoint
app.put('/greetings/:id', (req, res) => {
    const id = req.params.id;
    const message = req.body.message;
    console.log(`Received PUT request for id: ${id} with message: ${message}`);
    if (message) {
        greetings[id] = { message };
        res.status(200).send({ message: 'Greeting updated' });
    } else {
        res.status(400).send({ error: 'Message is required' });
    }
});

// DELETE /greetings/:id endpoint
app.delete('/greetings/:id', (req, res) => {
    const id = req.params.id;
    console.log(`Received DELETE request for id: ${id}`);
    if (greetings[id]) {
        delete greetings[id];
        res.status(200).send({ message: 'Greeting deleted' });
    } else {
        res.status(404).send({ error: 'Greeting not found' });
    }
});

// POST /greetings endpoint
app.post('/greetings', (req, res) => {
    const id = new Date().getTime().toString();
    const message = req.body.message;
    console.log(`Received POST request with message: ${message}`);
    if (message) {
        greetings[id] = { message };
        res.status(201).send({ id, message: 'Greeting created' });
    } else {
        res.status(400).send({ error: 'Message is required' });
    }
});

// Graceful shutdown
process.on('SIGTERM', () => {
    console.log('SIGTERM signal received. Closing HTTP server...');
    server.close(() => {
        console.log('HTTP server closed.');
    });
});
process.on('SIGINT', () => {
    console.log('SIGINT signal received. Closing HTTP server...');
    server.close(() => {
        console.log('HTTP server closed.');
    });
});
