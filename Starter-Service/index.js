const express = require('express');
const axios = require('axios');
const app = express();
const port = process.env.PORT || 3000;
app.use(express.json());

const callerServiceUrl = process.env.CALLER_SERVICE_URL || 'http://caller-service:80';

const triggerCallerService = async () => {
    try {
        const response = await axios.post(`${callerServiceUrl}/trigger_success`);
        console.log(response.data);
        return { success: true, message: 'Caller service triggered successfully!', data: response.data };
    } catch (error) {
        console.error('Error triggering caller-service:', error.message);
        return { success: false, message: 'Failed to trigger caller service' };
    }
};

// Endpoint to trigger the caller-service
app.post('/trigger', async (req, res) => {
    const result = await triggerCallerService();
    res.json(result);
});

// Proxy for /trigger_doublethree to call the corresponding endpoint in caller-service
app.get('/trigger_doublethree', async (req, res) => {
    try {
        const response = await axios.get(`${callerServiceUrl}/trigger_doublethree`);
        res.send(response.data);
    } catch (error) {
        console.error('Error calling /trigger_doublethree:', error.message);
        res.status(500).send('Error calling /trigger_doublethree');
    }
});

// Proxy for /trigger_doubleseven to call the corresponding endpoint in caller-service
app.get('/trigger_doubleseven', async (req, res) => {
    try {
        const response = await axios.get(`${callerServiceUrl}/trigger_doubleseven`);
        res.send(response.data);
    } catch (error) {
        console.error('Error calling /trigger_doubleseven:', error.message);
        res.status(500).send('Error calling /trigger_doubleseven');
    }
});

// Proxy for /triggerInconsistentNaming_caps
app.post('/triggerInconsistentNaming_caps', async (req, res) => {
    try {
        // Forward the request body to the caller-service
        const { myvar } = req.body;
        console.log(`Received POST request triggerInconsistentNaming_caps for: ${myvar}`);
        const response = await axios.post(`${callerServiceUrl}/triggerInconsistentNaming_caps`, req.body);
        res.status(200).send(response.data);
    } catch (error) {
        console.error('Error calling caller-service:', error.message);
        res.status(500).send('Error calling caller-service');
    }
});

// Proxy for /triggerInconsistentNaming_lower
app.post('/triggerInconsistentNaming_lower', async (req, res) => {
    try {
        // Forward the request body to the caller-service
        const { myvar } = req.body;
        console.log(`Received POST request triggerInconsistentNaming_lower for: ${myvar}`);
        const response = await axios.post(`${callerServiceUrl}/triggerInconsistentNaming_lower`, req.body);
        res.status(200).send(response.data);
    } catch (error) {
        console.error('Error calling caller-service:', error.message);
        res.status(500).send('Error calling caller-service');
    }
});

// Serve the static HTML page
// Moved this line down, after all API routes are defined
app.use(express.static('public'));

// Start the Express server
app.listen(port, () => {
    console.log(`Starter-Service listening at http://localhost:${port}`);
});
